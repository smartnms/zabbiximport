<?php

namespace Drupal\zabbiximport\Form;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\glpiinventory\glpi_api;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure glpi_inventory settings for this site.
 */
class zabbiximportSettingsForm extends ConfigFormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zabbiximport_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'zabbiximport.settings',
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('glpi_inventory.settings');

      $form['desdecero']= array(
          '#type' => 'fieldset',
          '#title' => $this->t('Import from scratch'),
      );
      $form['desdecero']['description']= array(
          '#markup' => '<h3>'.t('Use these options to import on a new fresh install, not to add new entries').'</h3>',
      );
      $form['desdecero']['importgroups'] = array(
          '#type' => 'checkbox',
          '#title' => $this->t('Import Zabbix Groups'),
          '#default_value' => FALSE,
        );

      $form['desdecero']['importhosts'] = array(
          '#type' => 'checkbox',
          '#title' => $this->t('Import Zabbix Hosts'),
          '#default_value' => FALSE,
      );

      $form['desdecero']['importtemplates'] = array(
          '#type' => 'checkbox',
          '#title' => $this->t('Import Zabbix Templates'),
          '#default_value' => FALSE,
      );

      $form['desdecero']['importtriggers'] = array(
          '#type' => 'checkbox',
          '#title' => $this->t('Import Zabbix Triggers'),
          '#default_value' => FALSE,
      );
    return parent::buildForm($form, $form_state);
  }
    /**
     * {@inheritdoc}
     */
    public function zabbiximport_groups(&$context){
        if (!isset($context['sandbox']['progress'])) {
            $context['sandbox']['progress'] = 0;
            $context['sandbox']['current_node'] = 0;
            $context['finished'] = 1;
        }
        $api=new zabbix_api();
        $result=$api->hostgroupGet(array('output'=>'extend'));
        foreach($result as $row=>$group) {
            $data[$row] = array(
                'type' => 'zabbixgroup_default',
                'group_id' => $group['groupid'],
                'name' => $group['name']
            );
        }
        foreach($data as $group) {
            $entity = \Drupal::entityManager()
                ->getStorage('zabbixgroup')
                ->create($group);
            $entity->save();
            drupal_set_message(t('Imported entity:').$entity->getGroupid().' '.$entity->getName() , 'status', TRUE);
        }
    }
    /**
     * {@inheritdoc}
     */
    public function zabbiximport_hosts(&$context){
        if (!isset($context['sandbox']['progress'])) {
            $context['sandbox']['progress'] = 0;
            $context['sandbox']['current_node'] = 0;
            $context['finished'] = 1;
        }
        $api=new zabbix_api();
        $result=$api->hostGet(array('output'=>'extend'));
        foreach($result as $row=>$host) {
            $data[$row] = array(
                'type' => 'zabbixhost_default',
                'host_id' => $host['hostid'],
                'name' => $host['name']
            );
        }
        $api=new glpi_api();
        $itemtype='Computer';
        foreach($data as $host) {
            $item=array('name'=>$host['name']);
            $createoutput=$api->createitem($itemtype,$item);
            $id=$createoutput['id'];
            $host['glpi_id']=$id;
            $entity = \Drupal::entityManager()
                ->getStorage('zabbixhost')
                ->create($host);
            $entity->save();
            drupal_set_message(t('Imported entity:').$entity->getHostid().' '.$entity->getName() , 'status', TRUE);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function zabbiximport_templates(&$context){
        if (!isset($context['sandbox']['progress'])) {
            $context['sandbox']['progress'] = 0;
            $context['sandbox']['current_node'] = 0;
            $context['finished'] = 1;
        }
        $api=new zabbix_api();
        $result=$api->templateGet(array('output'=>'extend'));
        foreach($result as $row=>$template) {
            $data[$row] = array(
                'type' => 'zabbixtemplate_default',
                'template_id' => $template['templateid'],
                'name' => $template['host'],
                'description' => $template['description'],
            );
        }
        foreach($data as $template) {
            $entity = \Drupal::entityManager()
                ->getStorage('zabbixtemplate')
                ->create($template);
            $entity->save();
            drupal_set_message(t('Imported entity:').$entity->getTemplateid().' '.$entity->getName() , 'status', TRUE);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function zabbiximport_triggers(&$context){
        if (!isset($context['sandbox']['progress'])) {
            $context['sandbox']['progress'] = 0;
            $context['sandbox']['current_node'] = 0;
            $context['finished'] = 1;
        }
        $api=new zabbix_api();
        $result=$api->triggerGet(array('output'=>array('triggerid','description','comments','functions','priority','expression'),'active'=>'true','selectFunctions'=> 'extend'));
        foreach($result as $row=>$trigger) {
            $data[$row] = array(
                'type' => 'zabbixtrigger_default',
                'trigger_id' => $trigger['triggerid'],
                'name' => $trigger['description'],
                'description' => $trigger['comments'],
                'function'=> $trigger['functions'][0]['function'],
                'parameter'=> $trigger['functions'][0]['parameter'],
                'severity'=>$trigger['priority'],
            );
            $inner_result=$api->itemGet(array('triggerids'=>[$trigger['triggerid']],'output'=>array('hostid','key_')));
            $data[$row]['server']=$inner_result['0']['hostid'];
            $data[$row]['key']=$inner_result['0']['key_'];
            $comparacion=array();
            if(preg_match("/{\d+}([^0-9]+)(\d+).*/",$trigger['expression'],$comparacion)){
                $data[$row]['operator']=$comparacion[1];
                $data[$row]['constant']=$comparacion[2];
            }
        }
        foreach($data as $trigger) {
            $entity = \Drupal::entityManager()
                ->getStorage('zabbixtrigger')
                ->create($trigger);
            $entity->save();
            drupal_set_message(t('Imported entity:').$entity->getTriggerid().' '.$entity->getName() , 'status', TRUE);
        }

    }
    /**
     * {@inheritdoc}
     */
    public function zabbiximportfinishedimport($success,$results,$operations) {
        if($success){
            drupal_set_message($this->t('Operation finished'), 'status', TRUE);
        }
        else {
            drupal_set_message($this->t('There was an error processing your request'), 'status', TRUE);
        }
    }
  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operations=array();

      if($form_state->getValue('importgroups'))
          $operations[]=array(array('Drupal\zabbiximport\Form\zabbiximportSettingsForm','zabbiximport_groups'),array());
      if($form_state->getValue('importhosts'))
          $operations[]=array(array('Drupal\zabbiximport\Form\zabbiximportSettingsForm','zabbiximport_hosts'),array());
      if($form_state->getValue('importtemplates'))
          $operations[]=array(array('Drupal\zabbiximport\Form\zabbiximportSettingsForm','zabbiximport_templates'),array());
      if($form_state->getValue('importtriggers'))
          $operations[]=array(array('Drupal\zabbiximport\Form\zabbiximportSettingsForm','zabbiximport_triggers'),array());
    $batch=array(
        'title' => t('Importing'),
        'operations' => $operations,
        'finished' => 'zabbiximportfinishedimport',
        'file' => 'Drupal\zabbiximport\Form\zabbiximportSettingsForm',
    );
    batch_set($batch);

    parent::submitForm($form, $form_state);
  }
}


?>