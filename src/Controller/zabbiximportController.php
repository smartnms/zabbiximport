<?php
/**
 * @file
 * Contains \Drupal\glpi_inventory\Controller.
 */
namespace Drupal\zabbiximport\Controller;
use Drupal\Core\Controller\ControllerBase;

class glpi_inventoryController {
	public function startAction() {
		return [
			'#markup' => '<h2>Welcome to the start page</h2>',
		];
	}
	public function settings() {
		return [
			'#markup' => '<h2>Página de importacion de Zabbix</h2>',
		];
	}
}
